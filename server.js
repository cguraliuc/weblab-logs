/**
 * @module server.js
 */

require('dotenv').config({
  silent: process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test'
})

// Express
const express = require('express')
const log = require('./lib/logger')
const server = express()

const bodyParser = require('body-parser')
const cors = require('cors')

// Weblab libs
const config = require('./lib/config')
const db = require('./lib/database/db')
const auth = require('./lib/auth/auth')

/**
 * Middlewares
 */

// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
server.use(bodyParser.json())
server.use(cors())

// Expose db, docker and models
server.use((req, res, next) => {
  req.log = log
  req.db = db()
  next()
})

/**
 * Routes
 */

// Need authentication for all routes pass this mark
server.all('/api/v1/logs/:user/*', auth)

// Get logs
server.get('/api/v1/logs/:user/:service/:version', require('./routes/v1/logs'))
server.get('/api/v1/logs/:user/:service/:version/browser', require('./routes/v1/browser'))
server.get('/api/v1/logs/:user/:service/:version/stream', require('./routes/v1/stream'))

// Error handler
server.use((err, req, res, next) => {
  req.log.error(err)

  const statusCode = err.statusCode || 500
  const payload = {
    name: err.name,
    message: err.message,
    stack: process.env.NODE_ENV !== 'production' ? err.stack : undefined
  }

  res.status(statusCode).json(payload)
})

/**
 * Run Server
 */

// Run server
const raw = server
  .listen(config.PORT, () => log.info('Listening at %s', config.PORT))

// Close server
const shutdown = require('./lib/shutdown')(raw)

// Graceful shutdown
process
  .on('SIGTERM', shutdown)
  .on('SIGINT', shutdown)
