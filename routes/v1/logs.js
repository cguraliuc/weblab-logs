const logs = (req, res, next) => {
  const user = req.params.user
  const service = req.params.service
  const version = req.params.version
  const query = req.query
  const collection = req.db.collection(`${user}.${service}.${version}`)

  const since = query.since
  const MAX_LINES = 300

  // Since logs
  if (since) {
    const limit = Math.max(parseInt(query.limit, 10) || 10, MAX_LINES)
    const sort = { $natural: 1 }

    return collection
      .find({ created: { $gte: since } })
      .limit(limit)
      .sort(sort)
      .toArray((err, data) => {
        if (err) return next(err)
        return res.send(data)
      })
  }

  // Tail logs
  const limit = Math.max(parseInt(query.tail, 10) || 10, MAX_LINES)
  const sort = { $natural: -1 }

  return collection
    .find()
    .limit(limit)
    .sort(sort)
    .toArray((err, data) => {
      if (err) return next(err)
      return res.send(data.reverse())
    })
}

module.exports = logs
