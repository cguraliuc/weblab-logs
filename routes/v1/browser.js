const fs = require('fs')
const path = require('path')
const html = fs.readFileSync(path.join(__dirname, './browser.html'))

const stream = (req, res, next) => {
  res.setHeader('Content-type', 'text/html')
  res.end(html)
}

module.exports = stream
