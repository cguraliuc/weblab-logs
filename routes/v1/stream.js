const zmq = require('zeromq')
const wsLogs = require('../../lib/ws/logs')

const stream = (req, res, next) => {
  const username = req.params.user
  const service = req.params.service
  const version = req.params.version

  let subscribers = []

  // Disable Nagle algorithm
  req.socket.setNoDelay(true)

  // Set event stream
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'X-Accel-Buffering': 'no'
  })

  // Send empty event to validate connection
  res.write(':ok\n\n')

  wsLogs
    .getPublisherIps()
    .then(ips => ips.forEach((ip) => {
      const subscriber = zmq.socket('sub')
      subscriber.subscribe(wsLogs.getLogCollectorTopic({ username, service, version }))
      subscriber.on('message', function message (topic, data) {
        res.write('data: ' + data.toString() + '\n\n')
      })
      subscriber.connect(`tcp://${ip}:7002`)
      subscribers.push(subscriber)
    }))
    .catch(next)

  function close () {
    subscribers.forEach(s => s.close())
    subscribers = []
  }

  res.on('close', close)
}

module.exports = stream
