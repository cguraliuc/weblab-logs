const crypto = require('crypto')
const request = require('superagent')

const config = require('../../lib/config')

let lastChecked = 0
let ips = []
const cachedFor = 60 * 1000 // 60 seconds

const removeDuplicates = (ips) => {
  const hash = {}

  ips.forEach((ip) => { hash[ip] = ip })

  return Object.keys(hash)
}

const getPublisherIps = () => {
  const now = Date.now()
  const diff = now - lastChecked

  if (diff > cachedFor) {
    lastChecked = now

    return request
      .get(`${config.CONSUL}/v1/catalog/service/${config.WL_FORWARDER}`)
      .then((res) => {
        const data = res.body
        const workers = Array.isArray(data) ? data : []
        const allIps = workers.map((w) => {
          const parts = w.ServiceID.split(':')
          const ip = parts[0].replace(/ip-/, '').replace(/-/g, '.')
          return ip
        })
        ips = removeDuplicates(allIps)
        return ips
      })
  }

  return Promise.resolve(ips)
}

const getLogCollectorTopic = ({ username, service, version }) => crypto
  .createHmac('sha256', config.WL_LOG_COLLECTOR_SECRET)
  .update(`${username}/${service}/${version}`)
  .digest('hex')

module.exports = {
  getPublisherIps,
  getLogCollectorTopic
}
