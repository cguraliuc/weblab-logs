const MongoClient = require('mongodb').MongoClient
const config = require('../config')
const log = require('../logger')

let connection

// Use connect method to connect to the Server
MongoClient.connect(config.MONGO_URL, (err, db) => {
  if (err) {
    log.error('[Mongo]: ', err)
    process.exit(1)
  }

  log.info('[Mongo] Connected')

  connection = db
})

module.exports = () => connection
