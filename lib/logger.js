const bunyan = require('bunyan')

module.exports = bunyan.createLogger({
  name: 'weblab/logs'
})
