const url = require('url')
const token = require('./token')

/**
 * Auth middleware for WS Server. Check `Authorization` header or `access_token` query param
 *
 * @param {Object} info - WS request info
 * @returns {Boolean} - True if request is authorized, false otherwie
 */
const auth = (info) => {
  const urlparts = url.parse(info.req.url, true)
  const query = urlparts.query
  const headers = info.req.headers
  const parts = (urlparts.pathname || '').split('/')

  const user = parts[4]
  // const service = parts[5]
  // const version = parts[6]
  const stream = parts[7] === 'ws'

  // Ignore non stream requests
  if (!stream) return false

  let accessToken

  // Authorization header
  if (headers['authorization']) {
    accessToken = (headers['authorization'].split(' '))[1]
  }

  // Access token
  if (query.access_token) {
    accessToken = query.access_token
  }

  let decoded = {}
  try {
    decoded = token.verify(accessToken)
  } catch (e) {
    return false
  }

  return decoded.username === user
}

module.exports = auth
