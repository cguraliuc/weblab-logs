module.exports = {
  PORT: process.env.WLB_PORT || 5003,
  JWT_SECRET: process.env.JWT_SECRET || '440044a39b964e67a8f199ebac13201c1b55e50809e34ecb85149bfe807b40aa',
  JWT_ISSUER: process.env.JWT_ISSUER || 'weblab-authenticator',
  MONGO_URL: process.env.MONGO_URL,
  CONSUL: process.env.CONSUL || '172.31.60.146:8500',
  WL_FORWARDER: process.env.WL_FORWARDER || 'log-collector-7002',
  WL_LOG_COLLECTOR_SECRET: process.env.WL_LOG_COLLECTOR_SECRET || 'c0fa1bc00531bd78ef38c628449c5102aeabd49b5dc3a2a516ea6ea959d6658e'
}
