const log = require('./logger')

// Let the process cooldown
const COOLDOWN_TIMEOUT = process.env.NODE_ENV === 'production' ? 500 : 0
// Force process kill after 5 seconds
const FORCE_KILL_TIMEOUT = process.env.NODE_ENV === 'production' ? 5000 : 0

// Kill server process
module.exports = function (server) {
  return function shutdown () {
    setTimeout(() => {
      log.info('Closing server')
      // Close server
      server.close(() => {
        log.info('Server closed')
        process.exit(0)
      })

      // Force kill after timeout
      setTimeout(() => {
        log.warn('Killing server')
        process.exit(0)
      }, FORCE_KILL_TIMEOUT)
    }, COOLDOWN_TIMEOUT)
  }
}
