FROM node:6.9.2-alpine
MAINTAINER Cristian Guraliuc <cristi@weblab.io>

# Add bash
RUN apk add --no-cache --update bash make gcc g++ python zeromq zeromq-dev

# Create app directory
RUN mkdir -p /opt/app
WORKDIR /opt/app

# Make user node owner
RUN chown -R node /opt/app

USER node

# Install app dependencies
COPY package.json /opt/app/
RUN npm install --production

# Bundle app source
COPY lib/ /opt/app/lib/
COPY routes/ /opt/app/routes/
COPY server.js /opt/app/

# Expose port
EXPOSE 5003

ENV NODE_ENV production

CMD ["node","server.js"]
